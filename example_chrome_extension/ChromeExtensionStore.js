/**
 * ChromeExtensionStore.js
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.04.24.
 * Copyright © 2024 Guardian Project. All rights reserved.
 */
const {Consents, Event, Store, StoreData, Visit} = require('clean-insights-sdk')

/**
 * A store using `chrome.storage` usable in Manifest v3 browser extensions,
 * where `localStorage` isn't available.
 *
 * See https://developer.chrome.com/docs/extensions/reference/api/storage
 */
class ChromeExtensionStore extends Store {

    static #storeJsKey = 'clean-insights'

    /**
     * @param {Object.<string, any>} args
     *      The location where to read and persist accumulated data.
     *      Either in the key "storageFile", which is expected to contain the
     *      fully qualified URL (as a string) to a file.
     *      Or a "storageDir" URL (as a string), which is expected to point
     *      to a directory.
     * @param {function(string)=}debug
     *      A callback to send debug messages to.
     */
    constructor(args, debug) {
        super(args, debug)

        chrome.storage.local.get(ChromeExtensionStore.#storeJsKey).then((items) => {
            let data = items[ChromeExtensionStore.#storeJsKey]
            data = JSON.parse(data)

            if (data && typeof data === 'object'
                && (
                    (data.consents && typeof data.consents === 'object')
                    || (data.visits && Array.isArray(data.visits))
                    || (data.events && Array.isArray(data.events)))
            ) {
                this.consents = new Consents(data.consents)

                data.visits.forEach((visit) => {
                    this.visits.push(new Visit(visit))
                })

                data.events.forEach((event) => {
                    this.events.push(new Event(event))
                })
            }
        })
    }

    /**
     * @param {Object.<string, any>} args
     * @return {undefined|StoreData}
     */
    load(args) {
        // Return empty, as this needs to be synchronous, which we cannot
        // do here. Instead, do async init in overridden constructor.
        return {consents: {features: {}, campaigns: {}}, visits: [], events: []}
    }

    /**
     * @param {boolean} async
     * @param {function(?Error=)} done
     */
    persist(async, done) {
        // This is always asynchronous.

        const data = {}
        data[ChromeExtensionStore.#storeJsKey] = JSON.stringify(this)

        chrome.storage.local.set(data).then(() =>  {
            if (async) {
                done()
            }
        })

        // We can only do async storage here, but let's call back immediately,
        // if the caller wishes that.
        if (!async) {
            done()
        }
    }

    /**
     * @param {string} data
     * @param {string} server
     * @param {number} timeout
     * @param {function(?Error=)} done
     */
    send(data, server, timeout, done) {
        const xhr= new XMLHttpRequest()

        xhr.onreadystatechange = () => {
            try {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status !== 200 && xhr.status !== 204) {
                        return done(new Error(`HTTP Error ${xhr.status}: ${xhr.responseText}`))
                    }

                    done()
                }
            } catch (e) {
                done(e)
            }
        }

        xhr.timeout = timeout * 1000

        xhr.open('POST', server)
        xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
        xhr.send(data)
    }
}

// export default ChromeExtensionStore
module.exports = ChromeExtensionStore
