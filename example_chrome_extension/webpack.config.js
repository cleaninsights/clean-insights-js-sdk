const path = require('path')

const {NODE_ENV = 'production'} = process.env

module.exports = {
    entry: './front_src/index.js',
    mode: NODE_ENV,
    output: {
        filename: 'main.js',
        path: __dirname
    },
    optimization: {
        minimize: false
    }
}
