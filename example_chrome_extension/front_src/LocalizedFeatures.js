/**
 * LocalizedFeatures.js
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
const {Feature} = require('clean-insights-sdk')

const LocalizedFeatures = {}
LocalizedFeatures[Feature.ua] = 'Your device type'
LocalizedFeatures[Feature.lang] = 'Your locale'

module.exports = LocalizedFeatures