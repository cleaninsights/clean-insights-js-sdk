/**
 * index.js
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
const start = Date.now()

const {CleanInsights} = require('clean-insights-sdk')
const ChromeExtensionStore = require('../ChromeExtensionStore')
const ConsentRequestUi = require('./ConsentRequestUi')
const LocalizedFeatures = require('./LocalizedFeatures')

const conf = {
    "server": "http://localhost:8080/ci/cleaninsights.php",
    "siteId": 1,
    "timeout": 5,
    "persistEveryNTimes": 1, // Always persist. Local storage should be fast enough to do that.
    "debug": true,
    "campaigns": {
        "test": {
            "start": "2022-01-01T00:00:00-00:00",
            "end": "2025-12-31T23:59:59-00:00",
            "aggregationPeriodLength": 1,
            "numberOfPeriods": 90,
            "onlyRecordOnce": false,
            "eventAggregationRule": "avg",
            "strengthenAnonymity": false
        }
    }
}

const ci = new CleanInsights(conf, new ChromeExtensionStore({}, (message) => { console.log(message) }))

window.onunload = () => {
    // Should be unnecessary, if you set "persistEveryNTimes" to 1,
    // but here for demonstration purposes, anyway.
    ci.persist()
}

console.log(ci)

window.root = () => {
    ci.measureVisit(['/'], 'test')

    console.log(ConsentRequestUi)

    const ui = new ConsentRequestUi()

    ci.requestConsentForCampaign('test', ui, (granted) => {
        if (!granted) {
            return
        }

        ci.requestConsentForFeature('lang', ui, () => {
            ci.requestConsentForFeature('ua', ui)
        })
    })
}

window.consents = () => {

    /**
     * @param {FeatureConsent} c
     * @return {HTMLTableRowElement}
     */
    const renderFeature = (c) => {
        const tr = document.createElement('tr')
        tr.innerHTML = `<td><label for="f_${c.feature}">${LocalizedFeatures[c.feature]}</label></td>
            <td><label for="f_${c.feature}">${c.start.format('LLL')}</label></td>
            <td><input id="f_${c.feature}" type="checkbox" name="${c.feature}" ${c.granted ? "checked" : ""}></td>`

        const checkbox = tr.getElementsByTagName('input')[0]

        checkbox.addEventListener('click', () => {
            let consent

            if (checkbox.checked) {
                consent = ci.grantFeature(checkbox.name)
            }
            else {
                consent = ci.denyFeature(checkbox.name)
            }

            document.getElementById('featureConsentsTable').getElementsByTagName('tbody')[0]
                .replaceChild(renderFeature(consent), tr)
        })

        return tr
    }

    /**
     * @param {CampaignConsent} c
     * @return {HTMLTableRowElement}
     */
    const renderCampaign = (c) => {
        const tr = document.createElement('tr')
        tr.innerHTML = `<td><label for="c_${c.campaignId}">${c.campaignId}</label></td>
            <td><label for="c_${c.campaignId}">${c.start.format('LLL')}</label></td>
            <td><label for="c_${c.campaignId}">${c.end.format('LLL')}</label></td>
            <td><input id="c_${c.campaignId}" type="checkbox" name="${c.campaignId}" ${c.granted ? "checked" : ""}></td>
`
        const checkbox = tr.getElementsByTagName('input')[0]

        checkbox.addEventListener('click', () => {
            let consent

            if (checkbox.checked) {
                consent = ci.grantCampaign(checkbox.name)
            }
            else {
                consent = ci.denyCampaign(checkbox.name)
            }

            document.getElementById('campaignConsentsTable').getElementsByTagName('tbody')[0]
                .replaceChild(renderCampaign(consent), tr)
        })

        return tr
    }

    let tbody = document.createElement('tbody')

    for (let i = 0; i < ci.featureConsents.length; i++) {
        tbody.appendChild(renderFeature(ci.getFeatureConsentByIndex(i)))
    }

    document.getElementById('featureConsentsTable').appendChild(tbody)

    tbody = document.createElement('tbody')

    for (let i = 0; i < ci.campaignConsents.length; i++) {
        tbody.appendChild(renderCampaign(ci.getCampaignConsentByIndex(i)))
    }

    document.getElementById('campaignConsentsTable').appendChild(tbody)
}

let time = Date.now() - start

ci.measureEvent('app-state', 'startup-success', 'test', 'time-needed', time)
