window.root()

const root = document.getElementById("root")

const consents = document.getElementById("consents")
consents.style.visibility = "hidden"

document.getElementById("toConsent").onsubmit = () => {
    root.style.visibility = "hidden"
    consents.style.visibility = "visible"

    let table = document.getElementById("featureConsentsTable")
    for (const tbody of table.getElementsByTagName("tbody")) {
        table.removeChild(tbody)
    }

    table = document.getElementById("campaignConsentsTable")
    for (const tbody of table.getElementsByTagName("tbody")) {
        table.removeChild(tbody)
    }

    window.consents()

    return false
}

document.getElementById("toRoot").onsubmit = () => {
    consents.style.visibility = "hidden"
    root.style.visibility = "visible"

    window.root()

    return false
}
