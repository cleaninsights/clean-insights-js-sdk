const path = require('path')

const {NODE_ENV = 'production'} = process.env

module.exports = {
    entry: './front_src/index.js',
    mode: NODE_ENV,
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'public')
    },
    optimization: {
        minimize: false
    }
}