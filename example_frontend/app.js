/**
 * app.js
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
const express = require('express')

const app = express()
app.use(express.static('public'))
const hostname = '127.0.0.1'
const port = 3000

app.get('/', (req, res) => {
    res.render('root.ejs')
})

app.get('/consents', (req, res) => {
    res.render('consents.ejs')
})


app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`)
})