/**
 * ConsentRequestUi.js
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
const cleanInsightsSdk = require('clean-insights-sdk')
const LocalizedFeatures = require('./LocalizedFeatures')

class ConsentRequestUi extends cleanInsightsSdk.ConsentRequestUi {

    showForCampaign(campaignId, campaign, complete) {
        const period = campaign.nextTotalMeasurementPeriod

        if (!period) {
            return ''
        }

        let message = 'Your Consent\n\n'
            + `We would like to run a measurement between ${period.start.format('LLL')} and ${period.end.format('LLL')} to understand some problems we noticed recently.\n\nYour help would be highly appreciated.`

        complete(window.confirm(message))

        return ''
    }

    showForFeature(feature, complete) {
        let message = 'Your Consent\n\n'
            + 'In case you allow us to run measurements, are you ok that we record the following?\n\n'
            + LocalizedFeatures[feature]

        complete(window.confirm(message))

        return ''
    }
}

module.exports = ConsentRequestUi
