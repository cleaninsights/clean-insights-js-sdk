# Clean Insights JavaScript SDK

Clean Insights gives developers a way to plug into a secure, private measurement platform. 
It is focused on assisting in answering key questions about app usage patterns, and not on 
enabling invasive surveillance of all user habits. 
Our approach provides programmatic levers to pull to cater to specific use cases and privacy needs. 
It also provides methods for user interactions that are ultimately empowering instead of alienating.

## Example

```JavaScript

    // Instantiate with configuration.
    const ci = CleanInsights({
        "server": "http://example.com/ci/cleaninsights.php",
        "siteId": 1,
        "campaigns": {
            "feature1-usage": {
                "start": "2021-01-01T00:00:00-00:00",
                "end": "2021-12-31T23:59:59-00:00",
                "aggregationPeriodLength": 1,
                "numberOfPeriods": 90
            }
        }
    })

    // Ask for consent:
    ci.requestConsentForCampaign("test", ConsentRequestUi())

    // Measure a page visit, e.g. in `#onload`:
    ci.measureVisit(["Main"], "test")

    // Measure an event (e.g. a button press):
    ci.measureEvent("music", "play", "test")

    // Make sure to persist the locally cached data. E.g. `onunload`.
    ci.persist()
```

*Please note*: `CleanInsights`' core concept is a **Campaign**. Since we don't want you to
record just *anything*, like all the others do, you need to configure, what your actually interested
in and for how long. For a deeper understanding, please read the [Concepts](#concepts) section below.

This project also contains different examples: One for a frontend app, one for a backend Node.js 
app and one for simpler websites. Please refer to the 
[GitLab repository](https://gitlab.com/cleaninsights/clean-insights-js-sdk) 
for a full understanding on how to use this library.

## Easy tracking of Websites with the AutoTracker Version

While the general `CleanInsights SDK` package is designed to be used in apps, with 
a lot of freedom for the developer in mind, we also provide a prepackaged, 
precompiled version to be used with normal websites:

[`clean-insights-auto-tracker.js`](https://gitlab.com/cleaninsights/clean-insights-js-sdk/-/blob/master/clean-insights-auto-tracker.js)
can just be dropped into any webpage and is used to automatically track page visits.

Additionally, you have a full `CleanInsights SDK` at hand and can do all the additional
tracking you can do with a normal version, too.

You still will need to provide consent. Check out the 
[website example](https://gitlab.com/cleaninsights/clean-insights-js-sdk/-/blob/master/example_website)
on how to do that in the most basic way.

### Caveat

The `CleanInsights SDK` has some important anonymizing features, which make the `AutoTracker` 
script less usable in certain circumstances:

Recording is only ever being sent when an aggregation period is over.
(Read the full document to get a better understanding of why.)

For the `AutoTracker`, that means, users need to visit on at least 2 days, since the 
aggregation period for the page visit tracking is 1 day: 

- On the first day, consent is given and visits are recorded.
- On the second day, visits of the first day are uploaded.
- (On the third day, visits of the second day will be uploaded, and so forth.)

These days don't need to be consecutive, but nevertheless, you need visitors who 
typically return.

That means, for *landing pages* and other sites which normally aren't visited multiple 
times, the `CleanInsights AutoTracker` isn't useful.

You can instead just use the standard `Matomo` tracker JavaScript or you could develop 
your own thing which works with the [CIMP](https://gitlab.com/cleaninsights/clean-insights-matomo-proxy).


## Installation of the standard SDK

CleanInsightsSDK is available through [npm](https://www.npmjs.com/package/clean-insights-sdk). 
To install it, execute one of the following commands in your project directory:

```shell
npm install --save clean-insights-sdk
# or
yarn add clean-insights-sdk
```

## Further Documentation:

- [Main project page](http://cleaninsights.org/)

- [All source code](https://gitlab.com/cleaninsights)

- [Design documents and JSON schema specifications](https://gitlab.com/cleaninsights/clean-insights-design)

- [API documentation](https://cleaninsights.gitlab.io/clean-insights-js-sdk/)

- [A Guide to Clean Consent UX](https://okthanks.com/blog/2021/5/14/clean-consent-ux)


## Concepts

### Measurements

A measurement is a single call to the SDK to measure a visit or an event.

Measurements are always aggregated immediately and only stored in that aggregated
form, in order to avoid a too high resolution and therefore unnecessary invasion into the users
privacy.

All measurements are done for a specific campaign you need to configure.

Measurements done against an unconfigured campaign are ignored, as well as measurements
done against a campaign which ran out or where the maximum length of days of data gathering
is crossed.

This helps you avoid unwanted measurements with left-over measurement code of older 
campaigns.


### Campaigns

A campaign has a period during which it is active and a maximum length of days during which
data is gathered for a specific user after they consented to the measurements.

The specific start date of a campaign helps you coordinate campaigns across platforms.
Measurements done before a campaign start are, of course, ignored.

The days during which measurements take place are defined by the aggregation period length
in days and the number of periods.

After a user consented, measurements will start right away for as long as the current measurement
period still goes on.

If you want to even further increase your users' anonymity guarantees and ensure that the first
measured period is a full one, you can configure that with the `strengthen_anonymity`
configuration option, which will enforce the beginning of measurements only at the next full
aggregation period.

At the end of an aggregation period, the campaign data will be automatically sent to your insights
server.

If you configure a higher `numberOfPeriods` than 1, the next aggregation period will begin
immediately after the end of the first one and measurement will continue.


#### Events

In Contrast to visit measurements, event measurements support the complete 
[Matomo Event API](https://matomo.org/docs/event-tracking/).

This means you can also record numeric values, like e.g. time something takes.

You can configure the aggregation method to use, when the same event is recorded multiple 
times: Event values can be summed up or an average can be calculated.

This can be configured per campaign.


#### Not a Matomo Campaign

Note the difference to Matomo: CleanInsights' concept of a campaign doesn't map to Matomo's
concept of campaigns, which is mostly about finding out if a marketing campaign (like an advertisement
or a landing page) sent any additional visitors to a website.


### Consent

You need to ask your users for consent to your measurements.

There are 2 types of consent:

- Campaign consents: Each measurement campaign needs to get consent by the user. You need
to explain to the user what you want to measure, how long you want to measure it and why.

The SDK contains UI to help you with that.

- Common feature consent: Some data is orthogonal to the visits or events you want to measure,
like locale or device type used. Since these features are only ever recorded when doing
measurements for a campaign, consent needs to be gathered only once per user per feature.

A user might want to actively withdraw consent. The SDK supports that. Please make sure that
the user can actually do that.

The SDK provides UI to help you with that.


## Configuration

To make sure your CleanInsights configuration is valid, you can use our 
[JSON scheme to validate against.](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schemas/configuration.schema.json)

Here's an [online validator](https://www.jsonschemavalidator.net)

A [complete documentation](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schema-docs/README.md)
is generated from that JSON scheme. 


## Supported Backends

CleanInsights SDK currently supports Matomo as a backend via 
[CIMP](https://gitlab.com/cleaninsights/clean-insights-matomo-proxy).


## Usage with Node.js

If you use this on the server side, we recommend to do only anonymous measurements.
That means, you should not measure anything which contains identifying information.

In that case, you won't need to ask anybody for consent, and you can make use of
the `serverSideAnonymousUsage` configuration option.

Check out the [backend example](example_backend) on how to configure the SDK and how 
to implement storage and upload to CIMP.

## Usage in a Chrome extension

If you want to use this in a Chrome extension, you can find an example implementation
and a usable store for `chrome.storage.local` in the
[example_chrome_extension](example_chrome_extension) folder.

## Usage with React Native

If you want to use this in React Native, you can find an example implementation
of a suitable store in [this example project](https://gitlab.com/cleaninsights/clean-insights-react-native-example).


## Author

Benjamin Erhart, berhart@netzarchitekten.com for the [Guardian Project](https://guardianproject.info)


## License

CleanInsightsSDK is available under the MIT license. See the LICENSE file for more info.
