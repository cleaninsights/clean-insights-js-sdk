/**
 * Event.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {Event, EventData}

import {DataPoint, DataPointData} from './DataPoint'

import dayjs from 'dayjs'

interface EventData extends DataPointData {
    category: string
    action: string
    name?: string
    value?: number
}

class Event extends DataPoint {

    category: string
    action: string
    name: string | undefined
    value: number | undefined

    /**
     * @param {string|EventData} category
     *      The event category or an object containing all arguments. Must not be empty. (eg. Videos, Music, Games...)
     *
     * @param {string=} action
     *      The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
     *      OPTIONAL if provided via an object as first argument.
     *
     * @param {string=} name
     *      The event name. OPTIONAL.
     *
     * @param {number=} value
     *      The event value. OPTIONAL.
     *
     * @param {string=} campaignId
     *      The campaign ID this data point is for.
     *      OPTIONAL if constructed with an object instead.
     *
     * @param {number=} times=1
     *      Number of times this data point has arisen between `first` and `last`. OPTIONAL.
     *
     * @param {dayjs.Dayjs=} first=NOW
     *      The first time this data point has arisen. OPTIONAL.
     *
     * @param {dayjs.Dayjs=} last=NOW
     *      The last time this data point has arisen. OPTIONAL.
     */
    constructor(category: string|EventData,
                action?: string, name?: string, value?: number, campaignId?: string, times?: number, first?: dayjs.Dayjs,
                last?: dayjs.Dayjs)
    {
        if (typeof category === 'string') {
            if (typeof campaignId === 'string' && typeof action === 'string') {
                super(campaignId, times, first, last)

                this.category = category
                this.action = action
                this.name = name
                this.value = value
            }
            else {
                throw TypeError('You either need to provide all non-optional arguments in their place or in an object as the first argument.')
            }
        }
        else {
            super(category.campaignId, category.times, category.first, category.last)

            this.category = category.category
            this.action = category.action
            this.name = category.name
            this.value = category.value
        }
    }

    toString() {
        return `[${this.constructor.name}: category=${this.category}, action=${this.action}, name=${this.name}, value=${this.value}, campaignId=${this.campaignId}, times=${this.times}, first=${this.first.format()}, last=${this.last.format()}]`
    }
}
