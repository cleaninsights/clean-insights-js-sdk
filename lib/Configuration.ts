/**
 * Configuration.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {Configuration, ConfigurationData}

import {Campaign, CampaignData} from "./Campaign";

interface ConfigurationData {
    server: string
    siteId: number
    campaigns: { [p: string]: CampaignData }
    timeout?: number
    maxRetryDelay?: number
    maxAgeOfOldData?: number
    persistEveryNTimes?: number
    serverSideAnonymousUsage?: boolean
    debug?: boolean
}

class Configuration {
    readonly server: string
    readonly siteId: number
    readonly campaigns: { [key: string]: Campaign }
    readonly timeout: number
    readonly maxRetryDelay: number
    readonly maxAgeOfOldData: number
    readonly persistEveryNTimes: number
    readonly serverSideAnonymousUsage: boolean
    readonly debug: boolean

    /**
     * @param {string} server
     *      The server URL should look like `https://myhost.example.com/ci/cleaninsights.php`.
     *
     * @param {number=} siteId
     *      The Matomo site ID to record this data for. OPTIONAL if provided via an object as first argument.
     *
     * @param {Object.<string, Campaign>=} campaigns
     *      Campaign configuration. OPTIONAL if provided via an object as first argument.
     *
     * @param {number=} timeout=5
     *      Connection timeout in seconds. OPTIONAL.
     *
     * @param {number=} maxRetryDelay=3600
     *      The SDK uses a truncated exponential backoff strategy on server failures.
     *      So the delay until it retries will rise exponentially, until it reaches
     *      `maxRetryDelay` seconds. OPTIONAL.
     *
     * @param {number=} maxAgeOfOldData=100
     *      The number in days of how long the SDK will try to keep sending old measurements.
     *      If the measurements become older than that, they will be purged. OPTIONAL.
     *
     * @param {number=} persistEveryNTimes=10
     *      Regulates, how often data persistence is done. OPTIONAL.
     *
     *      If set to 1, every time something is tracked, *ALL* data is stored to disk.
     *      The more you track, the higher you should set this to avoid heavy load due
     *      to disk I/O.
     *
     * @param {boolean=} serverSideAnonymousUsage=false
     *      When set to true, assumes consent for all campaigns and none for features.
     *      Only use this, when you're running on the server and don't measure anything users
     *      might need to give consent to!
     *
     * @param {boolean=} debug=false
     *      When set, CleanInsights SDK will print some debug output to STDOUT. OPTIONAL.
     */
    constructor(server: string|ConfigurationData, siteId?: number, campaigns?: {[p: string]: Campaign}, timeout?: number,
                maxRetryDelay?: number, maxAgeOfOldData?: number, persistEveryNTimes?: number,
                serverSideAnonymousUsage?: boolean, debug?: boolean)
    {
        if (typeof server === 'string') {
            if (!siteId || !campaigns) {
                throw TypeError('You either need to provide all non-optional arguments in their place or in an object as the first argument.')
            }

            this.server = server
            this.siteId = siteId
            this.campaigns = campaigns
            this.timeout = typeof timeout === 'number' ? timeout : 5
            this.maxRetryDelay = typeof maxRetryDelay === 'number' ? maxRetryDelay : 3600
            this.maxAgeOfOldData = typeof maxAgeOfOldData === 'number' ? maxAgeOfOldData : 100
            this.persistEveryNTimes = typeof persistEveryNTimes === 'number' ? persistEveryNTimes : 10
            this.serverSideAnonymousUsage = serverSideAnonymousUsage || false
            this.debug = debug || false
        }
        else {
            this.server = server.server
            this.siteId = server.siteId

            this.campaigns = {}

            for (const campaignId in server.campaigns) {
                if (server.campaigns.hasOwnProperty(campaignId)) {
                    this.campaigns[campaignId] = new Campaign(server.campaigns[campaignId])
                }
            }

            this.timeout = typeof server.timeout === 'number' ? server.timeout : 5
            this.maxRetryDelay = typeof server.maxRetryDelay === 'number' ? server.maxRetryDelay : 3600
            this.maxAgeOfOldData = typeof server.maxAgeOfOldData === 'number' ? server.maxAgeOfOldData : 100
            this.persistEveryNTimes = typeof server.persistEveryNTimes === 'number' ? server.persistEveryNTimes : 10
            this.serverSideAnonymousUsage = server.serverSideAnonymousUsage || false
            this.debug = server.debug || false
        }
    }

    toString() {
        return `[${this.constructor.name}: server=${this.server.toString()}, siteId=${this.siteId}, timeout=${this.timeout}, maxRetryDelay=${this.maxRetryDelay}, maxAgeOfOldData=${this.maxAgeOfOldData}, persistEveryNTimes=${this.persistEveryNTimes}, debug=${this.debug}, campaigns=${this.campaigns}]`
    }

    /**
     * Checks configuration for some well-known problems, emits a debug message and returns false, if one found.
     *
     * @param {function} debug
     *      Function to handle the debug message.
     * @return {boolean} `true`, if config seems ok, `false` if known problems exist.
     */
    check(debug: (message: string) => void): boolean {
        if (!this.server || !this.server.startsWith('http')) {
            debug("Configuration problem: 'server' is not defined properly. It needs to be a full URL like this: 'https://example.org/cleaninsights.php'!")

            return false
        }

        if (!this.siteId || this.siteId < 1) {
            debug("Configuration problem: 'siteId' is not defined properly. It needs to be a positive integer value!")

            return false
        }

        for (let c in this.campaigns) {
            if (this.campaigns.hasOwnProperty(c)) {
                return true
            }
        }

        debug("Configuration problem: No campaign defined!")

        return false
    }
}
