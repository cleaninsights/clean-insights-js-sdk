/**
 * BrowserStore.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {BrowserStore}

import {Store, StoreData} from './Store'

const storejs = require('store/dist/store.modern')

class BrowserStore extends Store {

    private static storeJsKey = 'clean-insights'


    load(args: { [p: string]: any }): undefined|StoreData {
        const data = storejs.get(BrowserStore.storeJsKey)

        if (data && typeof data === 'object'
            && (
                (data.consents && typeof data.consents === 'object')
                || (data.visits && Array.isArray(data.visits))
                || (data.events && Array.isArray(data.events)))
        ) {
            return data
        }

        return undefined
    }

    persist(async: boolean, callback: (error?: Error) => void): void {
        // We're not supporting web workers or the like in the browser environment, yet,
        // to do async storing.

        storejs.set(BrowserStore.storeJsKey, this)

        callback()
    }

    send(data: string, server: string, timeout: number, done: (error?: Error) => void): void {
        const xhr= new XMLHttpRequest()

        xhr.onreadystatechange = () => {
            try {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status !== 200 && xhr.status !== 204) {
                        return done(new Error(`HTTP Error ${xhr.status}: ${xhr.responseText}`))
                    }

                    done()
                }
            } catch (e: any) {
                done(e)
            }
        }

        xhr.timeout = timeout * 1000

        xhr.open('POST', server)
        xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
        xhr.send(data)
    }
}
