/**
 * consents.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {Feature, ConsentState, Consent, Consents, ConsentsData, ConsentData, ConsentsMap, FeatureConsent, CampaignConsent}

import {Campaign} from "./Campaign";

import dayjs from 'dayjs'

enum Feature {
    lang = "lang",
    ua = "ua"
}

enum ConsentState {

    /**
     A campaign with that ID doesn't exist or already expired.
     */
    unconfigured = "unconfigured",

    /**
     There's no record of consent. User was probably never asked.
     */
    unknown = "unknown",

    /**
     User denied consent. Don't ask again!
     */
    denied = "denied",

    /**
     Consent was given, but consent period has not yet started.
     */
    notStarted = "notStarted",

    /**
     Consent was given, but consent period is over. You might ask again for a new period.
     */
    expired = "expired",

    /**
     Consent was given and is currently valid.
     */
    granted = "granted"
}

interface ConsentData {
    granted: boolean
    start: string|dayjs.Dayjs
    end: string|dayjs.Dayjs
}

class Consent {

    granted: boolean
    start: dayjs.Dayjs
    end: dayjs.Dayjs

    /**
     * @param {boolean|ConsentData} granted
     * @param {dayjs.Dayjs=} start=NOW
     * @param {dayjs.Dayjs=} end=NOW
     */
    constructor(granted: boolean|ConsentData, start?: dayjs.Dayjs, end?: dayjs.Dayjs) {
        if (typeof granted === 'object' && granted) {
            this.granted = granted.granted
            this.start = dayjs(granted.start)
            this.end = dayjs(granted.end)
        }
        else {
            this.granted = granted
            this.start = start ? dayjs(start) : dayjs()
            this.end = end ? dayjs(end) : dayjs()
        }
    }

    get state(): ConsentState {
        return this.granted ? ConsentState.granted : ConsentState.denied
    }
}

class FeatureConsent extends Consent {

    feature: Feature|string

    /**
     * @param {Feature|string} feature
     * @param {Consent} consent
     */
    constructor(feature: Feature|string, consent: Consent) {
        super(consent.granted, consent.start, consent.end)

        this.feature = feature
    }
}

class CampaignConsent extends Consent {

    campaignId: string

    /**
     * @param {string} campaignId
     * @param {Consent} consent
     */
    constructor(campaignId: string, consent: Consent) {
        super(consent.granted, consent.start, consent.end)

        this.campaignId = campaignId
    }

    get state(): ConsentState {
        if (!this.granted) {
            return ConsentState.denied
        }

        const now = dayjs()

        if (now.isBefore(this.start)) {
            return ConsentState.notStarted
        }

        if (now.isAfter(this.end)) {
            return ConsentState.expired
        }

        return ConsentState.granted
    }
}

interface ConsentsMap {
    [key: string]: Consent|ConsentData
}

interface ConsentsData {
    features: ConsentsMap
    campaigns: ConsentsMap
}

/**
 * This class keeps track of all granted or denied consents of a user.
 *
 * There are two different types of consents:
 * - Consents for common features like if we're allowed to evaluate the locale or a user agent.
 * - Consents per measurement campaign.
 *
 * The time of the consent is recorded along with its state: If it was actually granted or denied.
 *
 * Consents for common features are given indefinitely, since they are only ever recorded along
 * with running campaigns.
 *
 * Consents for campaigns only last for a certain amount of days.
 */
class Consents {

    features: {[key: string]: Consent} = {}

    campaigns: {[key: string]: Consent} = {}

    /**
     *
     * @param {Object=}json Optional data from JSON deserialization to assign.
     */
    constructor(json?: {features: ConsentsMap, campaigns: ConsentsMap}) {

        if (typeof json === 'object' && json) {
            for (let feature in json.features) {
                if (json.features.hasOwnProperty(feature)) {
                    this.features[feature] = new Consent(json.features[feature])
                }
            }

            for (let campaign in json.campaigns) {
                if (json.campaigns.hasOwnProperty(campaign)) {
                    this.campaigns[campaign] = new Consent(json.campaigns[campaign])
                }
            }
        }
    }


    /**
     * User consents to evaluate a `Feature`.
     *
     * @param {Feature} feature
     * @returns {FeatureConsent}
     */
    grantFeature(feature: Feature): FeatureConsent {
        // Don't overwrite original grant timestamp.
        if (!this.features.hasOwnProperty(feature) || !this.features[feature].granted) {
            this.features[feature] = new Consent(true)
        }

        return new FeatureConsent(feature, this.features[feature])
    }

    /**
     * User denies consent to evaluate a `Feature`.
     *
     * @param {Feature} feature
     * @returns {FeatureConsent}
     */
    denyFeature(feature: Feature): FeatureConsent {
        // Don't overwrite original deny timestamp.
        if (!this.features.hasOwnProperty(feature) || this.features[feature].granted) {
            this.features[feature] = new Consent(false)
        }

        return new FeatureConsent(feature, this.features[feature])
    }

    /**
     * Returns the consent for a given feature, if any available.
     *
     * @param {Feature|string} feature The feature to get the consent for.
     * @returns {?FeatureConsent} the `FeatureConsent` for the given feature or `null`, if consent unknown.
     */
    consentForFeature(feature: Feature|string): FeatureConsent|null {
        if (!this.features.hasOwnProperty(feature)) {
            return null
        }

        return new FeatureConsent(feature, this.features[feature])
    }

    /**
     * Checks the consent state of a feature.
     *
     * @param {Feature} feature The feature to check the consent state of.
     * @returns {ConsentState} the current state of consent.
     */
    stateOfFeature(feature: Feature): ConsentState {
        return this.consentForFeature(feature)?.state ?? ConsentState.unknown
    }

    /**
     * User consents to run a specific campaign.
     *
     * @param {string} campaignId
     *      The campaign ID.
     * @param {Campaign} campaign
     *      The campaign.
     * @returns {CampaignConsent}
     */
    grantCampaign(campaignId: string, campaign: Campaign): CampaignConsent {
        const period = campaign.nextTotalMeasurementPeriod

        if (period) {
            // Always overwrite, since this might be a refreshed consent for a new period.
            this.campaigns[campaignId] = new Consent(true, period.start, period.end)
        }
        else {
            // Consent is technically granted, but has no effect, as start and end
            // will be set the same.
            this.campaigns[campaignId] = new Consent(true)
        }

        return new CampaignConsent(campaignId, this.campaigns[campaignId])
    }

    /**
     * User denies consent to run a specific campaign.
     *
     * @param {string} campaignId
     * @returns {CampaignConsent}
     */
    denyCampaign(campaignId: string): CampaignConsent {
        // Don't overwrite original deny timestamp.
        if (!this.campaigns.hasOwnProperty(campaignId) || this.campaigns[campaignId].granted) {
            this.campaigns[campaignId] = new Consent(false)
        }

        return new CampaignConsent(campaignId, this.campaigns[campaignId])
    }

    /**
     * Returns the consent for a given campaign, if any available.
     *
     * @param {string} campaignId The campaign ID to get the consent for.
     * @returns {?CampaignConsent} the `CampaignConsent` for the given campaign or `null`, if consent unknown.
     */
    consentForCampaign(campaignId: string): CampaignConsent|null {
        if (!this.campaigns.hasOwnProperty(campaignId)) {
            return null
        }

        return new CampaignConsent(campaignId, this.campaigns[campaignId])
    }

    /**
     * Checks the consent state of a campaign.
     *
     * @param {string} campaignId The campaign ID to check the consent state of.
     * @returns {ConsentState} the current state of consent.
     */
    stateOfCampaign(campaignId: string): ConsentState {
        return this.consentForCampaign(campaignId)?.state ?? ConsentState.unknown
    }
}
