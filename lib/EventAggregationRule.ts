/**
 * EventAggregationRule.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export { EventAggregationRule }

/**
 * The rule how to aggregate the value of an event (if any given) with
 * subsequent calls.
 *
 * @type {{Avg: string, Sum: string}}
 */
enum EventAggregationRule {
    /**
     * Calculate the sum of the values.
     */
    Sum = "sum",

    /**
     * Calculate the mean average of the values.
     */
    Avg = "avg"
}
