/**
 * ConsentRequestUi.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {ConsentRequestUi}

import {Campaign} from './Campaign'
import {Feature} from './consents'

/**
 * Implement this to provide a UI which explains your campaigns.
 *
 * Questions you should answer to the user:
 *
 * - Why do you need these measurements?
 * - How long will they get collected?
 * - What might attackers learn from these numbers?
 * - How long will you store them on your servers?
 * - What are you going to do with these measurements?
 * - How will the user benefit from these measurements?
 */
class ConsentRequestUi {

    /**
     * Will be called if it is necessary to ask the user for consent to a measurement campaign.
     *
     * @param {string} campaignId
     *      The campaign identifier.
     *
     * @param {Campaign} campaign
     *      The campaign configuration.
     *
     * @param {function(boolean)} complete
     *      The callback which will store the consent or the denial of it.
     * @returns {string} HTML output to show to the user, in case the user needs to get asked for consent.
     */
    showForCampaign(campaignId: string, campaign: Campaign, complete: (granted: boolean) => void): string {
        throw new Error('Override `ConsentRequestUi#showForCampaign` in your subclass!')
    }

    /**
     * Will be called if it is necessary to ask the user for consent to record a
     * common feature while measuring a campaign.
     *
     * @param {Feature} feature
     *      The feature to record. (e.g. user agent, locale)
     *
     * @param {function(boolean)} complete
     *      The callback which will store the consent or the denial of it.
     * @returns {string} HTML output to show to the user, in case the user needs to get asked for consent.
     */
    showForFeature(feature: Feature, complete: (granted: boolean) => void): string {
        throw new Error('Override `ConsentRequestUi#showForFeature` in your subclass!')
    }
}
