/**
 * DataPoint.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export { DataPoint, DataPointData }

import dayjs from 'dayjs'

interface DataPointData {
    campaignId: string
    times?: number
    first?: dayjs.Dayjs
    last?: dayjs.Dayjs
}

class DataPoint {

    campaignId: string
    times: number
    first: dayjs.Dayjs
    last: dayjs.Dayjs

    /**
     * @param {string} campaignId
     *      The campaign ID this data point is for.
     *
     * @param {number=} times=1
     *      Number of times this data point has arisen between `first` and `last`. OPTIONAL.
     *
     * @param {dayjs.Dayjs=} first=NOW
     *      The first time this data point has arisen. OPTIONAL.
     *
     * @param {dayjs.Dayjs=} last=NOW
     *      The last time this data point has arisen. OPTIONAL.
     */
    constructor(campaignId: string, times?: number, first?: dayjs.Dayjs, last?: dayjs.Dayjs) {
        this.campaignId = campaignId
        this.times = typeof times === 'number' ? times : 1
        this.first = first ? dayjs(first) : dayjs()
        this.last = last ? dayjs(last) : dayjs()
    }

    toString() {
        return `[${this.constructor.name}: campaignId=${this.campaignId}, times=${this.times}, first=${this.first.format()}, last=${this.last.format()}]`
    }
}
