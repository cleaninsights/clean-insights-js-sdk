/**
 * Store.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {Store, StoreData}

import {Consents, ConsentsData} from './consents'
import {Event, EventData} from './Event'
import {Visit, VisitData} from './Visit'

interface StoreData {
    consents: ConsentsData
    visits: [VisitData]
    events: [EventData]
}

/**
 * The store holds the user's consents to the different `Feature`s and `Campaign`s,
 * and their `Visit` and `Event` measurements.
 *
 * If you want to implement your own persistence of the store (e.g. because you
 * want to write it in a database instead of the file system) and your own
 * implementation of the transmission to the Matomo/CIMP backend (e.g. because
 * you want to tunnel the requests through a proxy or add your own encryption layer),
 * then create a subclass of this class and implement the `#constructor`,
 * `#persist` and `#send` methods.
 *
 * If you only want to change either one or the other, you can use `NodejsStore`
 * (found in the backend example) or `BrowserStore` as a base and work from there.
 */
abstract class Store {

    consents = new Consents()
    visits: Visit[] = []
    events: Event[] = []

    /**
     * @param {Object.<string, *>=} args={}
     *      Optional arguments your implementation might need for loading the store.
     * @param {function(string)=} debug=undefined
     *      Optional function to output debug messages.
     */
    constructor(args?: {[p: string]: any}, debug?: (message: string) => void) {
        if (typeof debug === 'undefined') {
            debug = () => {}
        }

        debug(`Store created from the following arguments: ${JSON.stringify(args)}`)

        args = args || {}
        args.debug = debug

        const data = this.load(args)

        if (typeof data === 'undefined') {
            debug("Storage doesn't exist or isn't readable.")
        }
        else {
            this.consents = new Consents(data.consents)

            data.visits.forEach((visit: VisitData) => {
                this.visits.push(new Visit(visit))
            })

            data.events.forEach((event: EventData) => {
                this.events.push(new Event(event))
            })

            debug('Data loaded from storage.')
        }
    }

    /**
     * This method gets called from the constructor and will receive the same
     * parameters, as the constructor got.
     *
     * @param {Object.<string: any>} args
     *      Arguments your implementation might need for loading the store.
     * @return {StoreData=} Deserialized store data.
     */
    abstract load(args: {[key: string]: any}): undefined|StoreData

    /**
     * This method gets called, when the SDK is of the opinion, that persisting
     * the `Store` is in order.
     *
     * This is partly controlled by the `Configuration.persistEveryNTimes` configuration
     * option, and by calls to `CleanInsights#persist`.
     *
     * If possible, try to honor the `async` flag:
     * If it's true, it is set so, because the SDK wants to reduce impact on user
     * responsivity as much as possible.
     * If it's false, the SDK wants to make sure, that the call finishes before
     * the app gets killed.
     *
     * @param {boolean} async
     *      Indicates, if the persistence should be done asynchronously or synchronously.
     *      E.g. a persist call during the exit of an app should be done synchronously,
     *      otherwise the operation might never get finished because the OS kills the
     *      server too early.
     * @param {function(?Error)} done
     *      Callback, when the operation is finished, either successfully or not.
     *      If no error is returned, the operation is considered successful and the
     *      internal counter will be set back again.
     */
    abstract persist(async: boolean, done: (error?: Error) => void): void

    /**
     * This method gets called, when `CleanInsights` gathered enough data for a
     * time period and is ready to send the data to a CIMP (CleanInsights Matomo Proxy).
     *
     * @param {string} data
     *      The serialized JSON for a POST request to a CIMP.
     * @param {string} server
     *      The server URL from `Configuration.server`.
     * @param {number} timeout
     *      The timeout in seconds from `Configuration.timeout`.
     * @param {function(?Error)} done
     *      Callback, when the operation is finished, either successfully or not.
     *      If no error is returned, the data sent will be removed from the
     *      store and the store persisted.
     */
    abstract send(data: string, server: string, timeout: number, done: (error?: Error) => void): void
}
