/**
 * CleanInsights.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {CleanInsights}

import {BrowserStore} from './BrowserStore'
import {Campaign} from './Campaign'
import {CampaignConsent, Consent, ConsentState, Feature, FeatureConsent} from './consents'
import {Configuration, ConfigurationData} from './Configuration'
import {ConsentRequestUi} from './ConsentRequestUi'
import {DataPoint} from './DataPoint'
import {Event} from './Event'
import {Insights} from './Insights'
import {Store} from './Store'
import {Visit} from './Visit'


import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import localizedFormat from 'dayjs/plugin/localizedFormat'

dayjs.extend(duration)
dayjs.extend(isSameOrBefore)
dayjs.extend(isSameOrAfter)
dayjs.extend(localizedFormat)

class CleanInsights {

    readonly conf: Configuration

    private readonly store: Store

    private persistenceCounter: number = 0

    private sending: boolean = false

    private failedSubmissionCount = 0
    private lastFailedSubmission = dayjs.unix(0)

    /**
     * @param {Object|Configuration} configuration
     *      The Configuration provided as a `Configuration` object or as a plain dictionary.
     *
     * @param {Store=} store=BrowserStore
     *      Either your own implementation of a `Store`. OPTIONAL.
     *      Defaults to `BrowserStore` which uses Store.js - an abstraction over LocalStorage.
     */
    constructor(configuration: Configuration|ConfigurationData, store?: Store|string)
    {
        if (typeof configuration === 'object') {
            if (configuration instanceof Configuration) {
                this.conf = configuration
            }
            else {
                this.conf = new Configuration(configuration)
            }
        }
        else {
            throw new TypeError('Invalid configuration provided.')
        }

        const debug = (message: string) => { this.debug(message) }

        if (!this.conf.check(debug)) {
            throw new TypeError('Invalid configuration provided.')
        }

        if (typeof store === 'object' && store) {
            this.store = store
        }
        else {
            this.store = new BrowserStore({}, debug)
        }
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Track a scene visit.
     *
     * @param {string[]} scenePath
     *      A hierarchical path best describing the structure of your scenes. E.g. `['Main', 'Settings', 'Some Setting']`.
     * @param {string} campaignId
     *      The campaign ID as per your configuration, where this measurement belongs to.
     */
    measureVisit(scenePath: string[], campaignId: string) {
        const campaign = this.getCampaignIfGood(campaignId, scenePath.join("/"))

        if (campaign) {
            let visit = this.getAndMeasure(this.store.visits, campaignId, campaign, (visit) => {
                return visit.scenePath.join('/') === scenePath.join('/')
            }) as Visit

            if (visit) {
                this.debug(`Gain visit insight: ${visit}`)
            }
            else {
                // Align first and last timestamps with campaign measurement period,
                // in order not to accidentally leak more information than promised.
                const period = campaign.currentMeasurementPeriod

                if (period) {
                    visit = new Visit(scenePath, campaignId, undefined, period.start, period.end)
                    this.store.visits.push(visit)

                    this.debug(`Gain visit insight: ${visit}`)
                }
                else {
                    this.debug("campaign.currentMeasurementPeriod == null! This should not happen!")
                }
            }
        }

        this.persistAndSend()
    }

    /**
     * Track an event.
     *
     * @param {string} category
     *      The event category. Must not be empty. (e.g. Videos, Music, Games...)
     * @param {string} action
     *      The event action. Must not be empty. (e.g. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
     * @param {string} campaignId
     *      The campaign ID as per your configuration, where this measurement belongs to.
     * @param {string=} name
     *      The event name. OPTIONAL.
     * @param {number=} value
     *      The event value. OPTIONAL.
     */
    measureEvent(category: string, action: string, campaignId: string, name?: string, value?: number) {
        const campaign = this.getCampaignIfGood(campaignId, `${category}/${action}`)

        if (campaign) {
            let event = this.getAndMeasure(this.store.events, campaignId, campaign, (event) => {
                return event.category === category
                    && event.action === action
                    && event.name === name
            }) as Event

            if (event) {
                campaign.apply(event, value)

                this.debug(`Gain event insight: ${event}`)
            }
            else {
                // Align first and last timestamps with campaign measurement period,
                // in order not to accidentally leak more information than promised.
                const period = campaign.currentMeasurementPeriod

                if (period) {
                    event = new Event(category, action, name, value, campaignId, undefined, period.start, period.end)

                    this.store.events.push(event)

                    this.debug(`Gain event insight: ${event}`)
                }
                else {
                    this.debug("campaign.currentMeasurementPeriod == null! This should not happen!")
                }
            }
        }

        this.persistAndSend()
    }

    get featureConsents(): string[] {
        return Object.keys(this.store.consents.features)
    }

    get campaignConsents(): string[] {
        return Object.keys(this.store.consents.campaigns)
    }

    getFeatureConsentByIndex(index: number): FeatureConsent|null {
        const feature = Object.keys(this.store.consents.features)[index]

        return this.store.consents.consentForFeature(feature)
    }

    getCampaignConsentByIndex(index: number): CampaignConsent|null {
        const campaignId = Object.keys(this.store.consents.campaigns)[index]

        return this.store.consents.consentForCampaign(campaignId)
    }

    grantFeature(feature: Feature): FeatureConsent {
        const consent = this.store.consents.grantFeature(feature)

        this.persistAndSend()

        return consent
    }

    denyFeature(feature: Feature): FeatureConsent {
        const consent = this.store.consents.denyFeature(feature)

        this.persistAndSend()

        return consent
    }

    /**
     * Returns the consent for a given feature, if any available.
     *
     * @param {Feature} feature The feature to get the consent for.
     * @returns {?FeatureConsent} the `FeatureConsent` for the given feature or `null`, if consent unknown.
     */
    consentForFeature(feature: Feature): FeatureConsent|null {
        if (this.conf.serverSideAnonymousUsage) return new FeatureConsent(feature, new Consent(false))

        return this.store.consents.consentForFeature(feature)
    }

    /**
     * Checks the consent state of a feature.
     *
     * @param {Feature} feature The feature to check the consent state of.
     * @returns {ConsentState} the current state of consent.
     */
    stateOfFeature(feature: Feature): ConsentState {
        if (this.conf.serverSideAnonymousUsage) return ConsentState.denied

        return this.store.consents.stateOfFeature(feature)
    }

    grantCampaign(campaignId: string): CampaignConsent|null {
        if (!this.conf.campaigns.hasOwnProperty(campaignId)) {
            return null
        }

        const campaign = this.conf.campaigns[campaignId]
        const consent = this.store.consents.grantCampaign(campaignId, campaign)

        this.persistAndSend()

        return consent
    }

    denyCampaign(campaignId: string): CampaignConsent|null {
        if (!this.conf.campaigns.hasOwnProperty(campaignId)) {
            return null
        }

        const consent = this.store.consents.denyCampaign(campaignId)

        this.persistAndSend()

        return consent
    }

    isCampaignCurrentlyGranted(campaignId: string): boolean {
        return this.stateOfCampaign(campaignId) == ConsentState.granted
    }

    /**
     * Returns the consent for a given campaign, if any available.
     *
     * @param {string} campaignId The campaign ID to get the consent for.
     * @return {?CampaignConsent} the `CampaignConsent` for the given campaign or `null`, if consent unknown.
     */
    consentForCampaign(campaignId: string): CampaignConsent|null {
        if (this.conf.serverSideAnonymousUsage) return new CampaignConsent(campaignId, new Consent(true))

        if (!this.conf.campaigns.hasOwnProperty(campaignId)) return null

        if (dayjs().isSameOrAfter(this.conf.campaigns[campaignId].end)) return null

        return this.store.consents.consentForCampaign(campaignId)
    }

    /**
     * Checks the consent state of a campaign.
     *
     * @param {string} campaignId The campaign ID to check the consent state of.
     * @return {ConsentState} the current state of consent.
     */
    stateOfCampaign(campaignId: string): ConsentState {
        if (this.conf.serverSideAnonymousUsage) return ConsentState.granted

        if (!this.conf.campaigns.hasOwnProperty(campaignId)) return ConsentState.unconfigured

        if (dayjs().isSameOrAfter(this.conf.campaigns[campaignId].end)) return ConsentState.unconfigured

        return this.store.consents.stateOfCampaign(campaignId)
    }

    requestConsentForCampaign(campaignId: string, consentRequestUi: ConsentRequestUi, completed?: (granted: boolean) => void): string {
        if (typeof completed !== 'function') {
            completed = () => { }
        }

        if (!this.conf.campaigns.hasOwnProperty(campaignId)) {
            this.debug(`Cannot request consent: Campaign '${campaignId}' not configured.`)

            completed(false)
            return ''
        }

        const campaign = this.conf.campaigns[campaignId]

        if (dayjs().isSameOrAfter(campaign.end)) {
            this.debug(`Cannot request consent: End of campaign '${campaignId}' reached.`)

            completed(false)
            return ''
        }

        if (campaign.nextTotalMeasurementPeriod === null) {
            this.debug(`Cannot request consent: Campaign '${campaignId}' configuration seems messed up.`)

            completed(false)
            return ''
        }

        if (this.store.consents.campaigns.hasOwnProperty(campaignId)) {
            const consent = this.store.consents.campaigns[campaignId]

            this.debug(`Already asked for consent for campaign '${campaignId}'. It was ${consent.granted ? `granted between ${consent.start.format()} and ${consent.end.format()}` : `denied on ${consent.start.format()}`}.`)

            completed(consent.granted)
            return ''
        }

        const complete = (granted: boolean) => {
            if (granted) {
                this.store.consents.grantCampaign(campaignId, campaign)
            }
            else {
                this.store.consents.denyCampaign(campaignId)
            }

            if (completed) {
                completed(granted)
            }
        }

        return consentRequestUi.showForCampaign(campaignId, campaign, complete)
    }

    requestConsentForFeature(feature: Feature, consentRequestUi: ConsentRequestUi, completed?: (granted: boolean) => void): string {
        if (typeof completed !== 'function') {
            completed = () => { }
        }

        if (this.store.consents.features.hasOwnProperty(feature)) {
            const consent = this.store.consents.features[feature]

            this.debug(`Already asked for consent for feature '${feature}'. It was ${consent.granted ? "granted" : "denied"} on ${consent.start.format()}.`)
            completed(consent.granted)
            return ''
        }

        const complete = (granted: boolean) => {
            if (granted) {
                this.store.consents.grantFeature(feature)
            }
            else {
                this.store.consents.denyFeature(feature)
            }

            if (completed) {
                completed(this.store.consents.stateOfFeature(feature) == ConsentState.granted)
            }
        }

        return consentRequestUi.showForFeature(feature, complete)
    }

    /**
     * Sends an empty body to the server for easy debugging of server-related issues like TLS and CORS problems.
     *
     * **DON'T LEAVE THIS IN PRODUCTION**, once you're done fixing any server issues. There's absolutely no point in
     * pinging the server with this all the time, it will undermine your privacy promise to your users!
     *
     * @param {function(?Error)} done
     *      Callback, when the operation is finished, either successfully or not.
     */
    testServer(done?: (error?: Error) => void) {
        this.store.send('', this.conf.server, this.conf.timeout, (error?: Error) => {
            if (!error) {
                error = new Error('Server replied with no error while it should have responded with HTTP 400 Bad Request!')
            }

            if (error.message.startsWith('HTTP Error 400:')) {
                this.debug('Successfully tested server.')

                if (done) done()

                return
            }

            this.debug(error)

            if (done) done(error)
        })
    }

    /**
     * Persist accumulated data to the filesystem/local storage.
     *
     * A website should call this in an `onUnload` event, a Node.JS app should call this when the
     * process exits for whatever reason. (See [node-cleanup](https://github.com/jtlapp/node-cleanup)).
     */
    persist() {
        this._persist(false, true)
    }

    /**
     * Persist accumulated data to the filesystem/local storage.
     *
     * @param {boolean} async
     *      If true, returns immediately and does persistence asynchronously, only if it's already due.
     *
     * @param {boolean=} force=false
     *      Write regardless of threshold reached.
     */
    private _persist(async: boolean, force?: boolean) {
        this.persistenceCounter += 1

        const callback = (error: Error|undefined) => {
            if (error) {
                this.debug(error)
            }
            else {
                this.persistenceCounter = 0

                this.debug("Data persisted to storage.")
            }
        }

        if (force || this.persistenceCounter >= this.conf.persistEveryNTimes) {
            this.store.persist(async, callback)
        }
    }

    private persistAndSend() {
        this._persist(true)

        if (this.sending) {
            this.debug("Data sending already in progress.")

            return
        }

        this.sending = true

        if (this.failedSubmissionCount > 0) {
            // Calculate a delay for the next retry:
            // Minimum is 2 times the configured network timeout after the first failure,
            // exponentially increasing with number of retries.
            // Maximum is every conf.maxRetryDelay interval.
            const exp = this.lastFailedSubmission.add(this.conf.timeout * Math.pow(2, this.failedSubmissionCount), 'seconds')
            const tru = this.lastFailedSubmission.add(this.conf.maxRetryDelay, 'seconds')

            if (dayjs().isBefore(exp.isBefore(tru) ? exp : tru)) {
                this.sending = false

                this.debug(`Waiting longer to send data after ${this.failedSubmissionCount} failed attempts.`)

                return
            }
        }

        const insights = new Insights(this.conf, this.store)

        if (insights.isEmpty) {
            this.sending = false

            this.debug("No data to send.")

            return
        }

        const done = (error?: Error) => {
            if (error) {
                this.lastFailedSubmission = dayjs()
                this.failedSubmissionCount ++

                this.debug(error)
            }
            else {
                this.lastFailedSubmission = dayjs.unix(0)
                this.failedSubmissionCount = 0

                insights.clean(this.store)

                this.debug('Successfully sent data.')

                this._persist(true, true)
            }

            this.sending = false
        }

        const data = JSON.stringify(insights)

        this.store.send(data, this.conf.server, this.conf.timeout, done)
    }

    getCampaignIfGood(campaignId: string, debugString: string): Campaign|null {
        if (!this.conf.campaigns.hasOwnProperty(campaignId)) {
            return null
        }

        const campaign = this.conf.campaigns[campaignId]

        const now = dayjs()

        if (now.isBefore(campaign.start)) {
            this.debug(`Measurement '${debugString}' discarded, because campaign '${campaignId}' didn't start, yet.`)
            return null
        }

        if (now.isAfter(campaign.end)) {
            this.debug(`Measurement '${debugString}' discarded, because campaign '${campaignId}' already ended.`)
            return null
        }

        if (!this.isCampaignCurrentlyGranted(campaignId)) {
            this.debug(`Measurement '${debugString}' discarded, because campaign '${campaignId}' has no user consent yet, any more or we're outside the measurement period.`)
            return null
        }

        return campaign
    }

    /**
     * Get a `DataPoint` subclass out of the `haystack`, as long as it fits the `campaign`.
     * Increases `times` according to the campaigns rules.
     *
     * Create a new `DataPoint` if nothing is returned here.
     *
     * @param {DataPoint[]|Visit[]|Event[]} haystack
     *      The haystack full of `DataPoint` subclasses.
     *
     * @param {string} campaignId
     *      The campaign ID it must match.
     *
     * @param {Campaign} campaign
     *      The campaign parameters to match against.
     *
     * @param {function(DataPoint):boolean} where
     *      Additional condition for selection.
     *
     * @returns: {?(DataPoint|Visit|Event)} a `DataPoint` subclass out of the `haystack`, as long as it fits the `campaign`.
     */
    private getAndMeasure<T extends DataPoint>(haystack: T[], campaignId: string, campaign: Campaign, where: (dataPoint: T) => boolean) {

        const period = campaign.currentMeasurementPeriod

        if (!period) {
            this.debug("campaign.currentMeasurementPeriod == null! This should not happen!")
            return null
        }

        const dataPoint = haystack.find((value) => {
            return value.campaignId === campaignId
                && value.first.isSameOrAfter(period.start)
                && value.first.isSameOrBefore(period.end)
                && value.last.isSameOrAfter(period.start)
                && value.last.isSameOrBefore(period.end)
                && where(value)
        })

        if (dataPoint instanceof DataPoint) {
            if (!campaign.onlyRecordOnce) {
                dataPoint.times += 1
            }

            return dataPoint
        }

        return null
    }

    private debug(message: string|Error) {
        if (!this.conf.debug) {
            return
        }

        if (typeof message === 'object' && message) {
            console.debug(`[CleanInsightsSDK] ${message.name}: ${message.message}\n\n${message.stack}`)
        }
        else {
            console.debug(`[CleanInsightsSDK] ${message}`)
        }
    }
}
