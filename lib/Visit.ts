/**
 * Visit.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {Visit, VisitData}

import {DataPoint, DataPointData} from "./DataPoint"

import dayjs from 'dayjs'

interface VisitData extends DataPointData {
    scenePath: string[]
}

class Visit extends DataPoint {
    scenePath: string[]

    /**
     * @param {string[]|VisitData} scenePath
     *      A hierarchical path to the scene visited or an object containing all arguments.
     *
     * @param {string=} campaignId
     *      The campaign ID this data point is for. OPTIONAL if provided via an object as first argument.
     *
     * @param {number=} times=1
     *      Number of times this data point has arisen between `first` and `last`. OPTIONAL.
     *
     * @param {dayjs.Dayjs=} first=NOW
     *      The first time this data point has arisen. OPTIONAL.
     *
     * @param {dayjs.Dayjs=} last=NOW
     *      The last time this data point has arisen. OPTIONAL.
     */
    constructor(scenePath: string[]|VisitData, campaignId?: string, times?: number, first?: dayjs.Dayjs, last?: dayjs.Dayjs) {
        if (Array.isArray(scenePath)) {
            if (typeof campaignId === 'string') {
                super(campaignId, times, first, last)

                this.scenePath = scenePath
            }
            else {
                throw TypeError('You either need to provide all non-optional arguments in their place or in an object as the first argument.')
            }
        }
        else {
            super(scenePath.campaignId, scenePath.times, scenePath.first, scenePath.last)

            this.scenePath = scenePath.scenePath
        }
    }

    toString() {
        return `[${this.constructor.name}: scenePath=${this.scenePath}, campaignId=${this.campaignId}, times=${this.times}, first=${this.first.format()}, last=${this.last.format()}]`
    }
}
