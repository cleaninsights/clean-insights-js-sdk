/**
 * Campaign.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export { Campaign, CampaignData }

import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore'

dayjs.extend(duration)
dayjs.extend(isSameOrBefore)

import {EventAggregationRule} from './EventAggregationRule'
import {Event} from './Event'

interface CampaignData {
    start: string
    end: string
    aggregationPeriodLength: number
    numberOfPeriods?: number
    onlyRecordOnce?: boolean
    eventAggregationRule?: string
    strengthenAnonymity?: boolean
}

class Campaign {

    start: dayjs.Dayjs
    end: dayjs.Dayjs
    aggregationPeriodLength: number
    numberOfPeriods: number
    onlyRecordOnce: boolean
    eventAggregationRule: EventAggregationRule
    strengthenAnonymity: boolean

    /**
     * @param {dayjs.Dayjs|CampaignData} start
     *      The start of the campaign. (inclusive)
     *
     * @param {dayjs.Dayjs=} end
     *      The end of the campaign. (inclusive) OPTIONAL if provided via an object as first argument.
     *
     * @param {number=} aggregationPeriodLength
     *      The length of the aggregation period in number of days. At the end of a period,
     *      the aggregated data will be sent to the analytics server.
     *      OPTIONAL if provided via an object as first argument.
     *
     * @param {number=} numberOfPeriods=1
     *      The number of periods you want to measure in a row. Therefore the total
     *      length in days you measure one user is `aggregationPeriodLength * numberOfPeriods`
     *      beginning with the first day of the next period after the user consented.
     *
     * @param {boolean=} onlyRecordOnce=false
     *      Will result in recording only the first time a visit or event happened
     *      per period. Useful for yes/no questions.
     *
     * @param {EventAggregationRule=} eventAggregationRule=.Sum
     *      The rule how to aggregate the value of an event (if any given)
     *      with subsequent calls.
     *
     * @param {boolean=} strengthenAnonymity=false
     *      When set to true, measurements only ever start at the next full period.
     *      This ensures, that anonymity guaranties aren't accidentally reduced because the
     *      first period is very short.
     */
    constructor(start: dayjs.Dayjs|CampaignData, end?: dayjs.Dayjs, aggregationPeriodLength?: number, numberOfPeriods?: number,
                onlyRecordOnce?: boolean, eventAggregationRule?: EventAggregationRule, strengthenAnonymity?: boolean)
    {
        if ('isValid' in start) { // Just checks for an arbitrary Day.js function, because instanceof test throws.
            if (!end || !aggregationPeriodLength) {
                throw TypeError('You either need to provide all non-optional arguments in their place or in an object as the first argument.')
            }

            this.start = start
            this.end = end
            this.aggregationPeriodLength = aggregationPeriodLength
            this.numberOfPeriods = typeof numberOfPeriods === 'number' ? numberOfPeriods : 1
            this.onlyRecordOnce = onlyRecordOnce || false
            this.eventAggregationRule = eventAggregationRule || EventAggregationRule.Sum
            this.strengthenAnonymity = strengthenAnonymity || false
        }
        else {
            this.start = dayjs(start.start)
            this.end = dayjs(start.end)
            this.aggregationPeriodLength = start.aggregationPeriodLength
            this.numberOfPeriods = typeof start.numberOfPeriods === 'number' ? start.numberOfPeriods : 1
            this.onlyRecordOnce = start.onlyRecordOnce || false
            this.eventAggregationRule = start.eventAggregationRule === "avg" ? EventAggregationRule.Avg : EventAggregationRule.Sum
            this.strengthenAnonymity = start.strengthenAnonymity || false
        }
    }

    get aggregationPeriod(): duration.Duration {
        return dayjs.duration(this.aggregationPeriodLength, 'days')
    }

    /**
     * Returns the current measurement period, aka. the period where NOW is in.
     *
     * If NOW is outside any possible period, because the campaign hasn't started,
     * yet, or already ended, will return `null`.
     *
     * The first period is defined as `aggregationPeriodLength` number of days after
     * the `start` of the campaign.
     *
     * @returns {?{start: dayjs.Dayjs, end: dayjs.Dayjs}}
     */
    get currentMeasurementPeriod(): {start: dayjs.Dayjs, end: dayjs.Dayjs}|null {
        if (this.numberOfPeriods <= 0) {
            return null
        }

        let now = dayjs()

        let periodEnd = this.start

        do {
            periodEnd = periodEnd.add(this.aggregationPeriod)
        } while (periodEnd.isSameOrBefore(now))

        let periodStart = periodEnd.subtract(this.aggregationPeriod).isAfter(this.start)
            ? periodEnd.subtract(this.aggregationPeriod)
            : this.start

        if (periodEnd.isAfter(this.end)) {
            periodEnd = this.end
        }

        now = dayjs()

        if (periodStart.isAfter(now) || periodEnd.isBefore(now)) {
            return null
        }

        return {start: periodStart, end: periodEnd}
    }

    /**
     * @returns {?{start: dayjs.Dayjs, end: dayjs.Dayjs}}
     */
    get nextTotalMeasurementPeriod(): {start: dayjs.Dayjs, end: dayjs.Dayjs}|null {
        const current = this.currentMeasurementPeriod

        if (!current) {
            return null
        }

        const periodStart = this.strengthenAnonymity ? current.end : current.start
        let periodEnd = periodStart

        let counter = 0

        while (counter < this.numberOfPeriods && periodEnd.add(this.aggregationPeriod) <= this.end) {
            periodEnd = periodEnd.add(this.aggregationPeriod)
            counter += 1
        }

        if (periodStart.isSame(periodEnd)) {
            return null
        }

        return {start: periodStart, end: periodEnd}
    }


    /**
     * Apply the `eventAggregationRule` to the given event with the given value.
     *
     * @param {Event} event
     *      The event to apply the value to.
     *
     * @param {number=} value
     *      The value to apply.
     */
    apply(event: Event, value?: number) {
        if (this.onlyRecordOnce || typeof value !== 'number') {
            return
        }

        const oldVal = typeof event.value === 'number' ? event.value : 0

        switch (this.eventAggregationRule) {
            case "sum":
                event.value = oldVal + value
                break

            case "avg":
                // times was already increased in CleanInsights#getAndMeasure!
                event.value = (oldVal * (event.times - 1) + value) / event.times
                break
        }
    }

    toString() {
        return `[${this.constructor.name}: start=${this.start}, end=${this.end}, aggregationPeriodLength=${this.aggregationPeriodLength}, numberOfPeriods=${this.numberOfPeriods}, onlyRecordOnce=${this.onlyRecordOnce}, eventAggregationRule=${this.eventAggregationRule}]`
    }
}
