/**
 * Insights.ts
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
export {Insights}

import {Configuration} from './Configuration'
import {Consents, ConsentState, Feature} from './consents'
import {DataPoint} from './DataPoint'
import {Event} from './Event'
import {Store} from './Store'
import {Visit} from './Visit'

import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'

dayjs.extend(duration)

interface DataPointData {
    period_start: number
    period_end: number
    times: number
}

interface VisitData extends DataPointData {
    action_name: string
}

interface EventData extends DataPointData {
    category: string
    action: string
    name?: string
    value?: number
}

class Insights {

    /**
     * Matomo site ID.
     */
    idsite: number = 0

    /**
     * Preferred user languages as an HTTP Accept header.
     */
    lang?: string = undefined

    /**
     * User Agent string.
     */
    ua?: string = undefined

    /**
     * `Visit` data points.
     */
    visits: VisitData[] = []

    /**
     * `Event` data points.
     */
    events: EventData[] = []

    /**
     * Create an `Insights` object according to configuration with all data from the store which
     * is due for offloading to the server.
     *
     * @param {Configuration} conf
     *      The current configuration.
     *
     * @param {{consents: Consents, visits: Visit[], events: Event[]}} store
     *      The current measurement and consents store.
     */
    constructor(conf: Configuration, store: Store) {

        this.idsite = conf.siteId

        if (!conf.serverSideAnonymousUsage
            && store.consents.stateOfFeature(Feature.lang) == ConsentState.granted
            && typeof navigator === 'object' && navigator
        ) {
            this.lang = Insights.acceptLanguages()
        }

        if (!conf.serverSideAnonymousUsage
            && store.consents.stateOfFeature(Feature.ua) == ConsentState.granted
            && typeof navigator === 'object' && navigator
        ) {
            this.ua = navigator.userAgent
        }

        Insights.purge(conf, store)

        let now = dayjs()

        store.visits.forEach((visit) => {
            if (!conf.campaigns.hasOwnProperty(visit.campaignId)) {
                return
            }

            // Only send, after aggregation period is over. `last` should contain that date!
            if (now.isAfter(visit.last)) {
                this.visits.push({
                    action_name: visit.scenePath.join('/'),
                    period_start: visit.first.unix(),
                    period_end: visit.last.unix(),
                    times: visit.times
                })
            }
        })

        store.events.forEach((event) => {
            if (!conf.campaigns.hasOwnProperty(event.campaignId)) {
                return
            }

            // Only send, after aggregation period is over. `last` should contain that date!
            if (now.isAfter(event.last)) {
                this.events.push({
                    category: event.category,
                    action: event.action,
                    name: event.name,
                    value: event.value,
                    period_start: event.first.unix(),
                    period_end: event.last.unix(),
                    times: event.times
                })
            }
        })
    }

    /**
     * @returns {boolean}
     */
    get isEmpty() {
        return this.visits.length < 1 && this.events.length < 1
    }

    /**
     * Removes all visits and events from the given `Store`, which are also available in here.
     *
     * This should be called, when all `Insights` were offloaded at the server successfully.
     *
     * @param {{consents: Consents, visits: Visit[], events: Event[]}} store
     *      The store where the `Visit`s and `Event`s in here came from.
     */
    clean(store: Store) {
        this.visits.forEach((d) => {
            let i = store.visits.length

            while (i--) {
                const v = store.visits[i]

                if (v.scenePath.join('/') === d.action_name && Insights.equals(v, d)) {
                    store.visits.splice(i, 1)
                }
            }
        })

        this.events.forEach((d) => {
            let i = store.events.length

            while (i--) {
                const e = store.events[i]

                // noinspection EqualityComparisonWithCoercionJS
                if (e.category === d.category && e.action === d.action
                    && e.name == d.name && e.value == d.value && Insights.equals(e, d))
                {
                    store.events.splice(i, 1)
                }
            }
        })
    }

    /**
     *
     * @param {DataPoint} dp
     * @param {{period_start: number, period_end: number, times: number}} data
     * @return {boolean}
     */
    private static equals(dp: DataPoint, data: DataPointData): boolean {
        return dp.first.unix() === data.period_start && dp.last.unix() === data.period_end && dp.times === data.times
    }

    /**
     * Create an HTTP-accept-language-header-like string from the information
     * we can get from inside JavaScript.
     *
     * @private
     * @return {string}
     */
    private static acceptLanguages() {
        let components = []

        let languages = navigator.languages || [navigator.language]

        for (let i = 0; i < languages.length; i++) {
            let quality = 1.0 - (i * 0.1)

            components.push(`${languages[i]};q=${quality}`)

            if (quality <= 0.5) {
                break
            }
        }

        return components.join(',')
    }

    /**
     Removes `DataPoint`s, which are too old. These were never been sent, otherwise, they would have
     been removed, already.

     Remove them now, if they're over the threshold, to not accumulate too many `DataPoints` and
     therefore reduce privacy.
     */
    protected static purge(conf: Configuration, store: Store) {
        const threshold = dayjs().subtract(dayjs.duration(conf.maxAgeOfOldData, 'days'))

        let i = store.visits.length

        while (i--) {
            if (store.visits[i].last.isBefore(threshold)) {
                store.visits.splice(i, 1)
            }
        }

        i = store.events.length

        while (i--) {
            if (store.events[i].last.isBefore(threshold)) {
                store.events.splice(i, 1)
            }
        }
    }
}
