/**
 * app.js
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
const start = Date.now()

const {CleanInsights} = require('clean-insights-sdk')

const fs = require('fs')

const nodeCleanup = require('node-cleanup')
const express = require('express')

const NodejsStore = require('./NodejsStore')


const conf = JSON.parse(fs.readFileSync('./cleaninsights.json', 'utf8'))

const ci = new CleanInsights(conf, new NodejsStore({}, debug))

debug(ci)

const app = express()
const hostname = '127.0.0.1'
const port = 3000

app.get('/', (req, res) => {
    ci.measureVisit(['/'], 'test')

    res.render('root.ejs', {ci: ci})
})

// Make sure recorded measurements are persisted before exit.
nodeCleanup(() => {
    ci.persist()
})

app.listen(port, hostname, () => {
    debug(`Server running at http://${hostname}:${port}/`)

    let time = Date.now() - start

    ci.measureEvent('app-state', 'startup-success', 'test', 'time-needed', time)
})

function debug(message) {
    if (typeof message === 'string') {
        return console.debug(`[App] ${message}`)
    }

    console.debug(message)
}