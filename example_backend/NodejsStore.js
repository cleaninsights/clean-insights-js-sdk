/**
 * NodejsStore.js
 * CleanInsightsSDK
 *
 * Created by Benjamin Erhart on 19.01.21.
 * Copyright © 2021 Guardian Project. All rights reserved.
 */
const {Store, StoreData} = require('clean-insights-sdk')

const fs = require('fs')
const http = require('http')

class NodejsStore extends Store {

    static #storageFilename = 'cleaninsights-store.json'

    /**
     * @type {string}
     */
    #storageFile

    /**
     * @param {Object.<string, any>} args
     *      The location where to read and persist accumulated data.
     *      Either in the key "storageFile", which is expected to contain the
     *      fully qualified URL (as a string) to a file.
     *      Or a "storageDir" URL (as a string), which is expected to point
     *      to a directory.
     * @param {function(string)=}debug
     *      A callback to send debug messages to.
     */
    constructor(args, debug) {
        // Support lightweight subclasses or invocations which only want
        // to override the storageFile location.
        if (typeof args.storageFile !== 'string') {
            args.storageFile = './'

            if (typeof args.storageDir === 'string') {
                args.storageFile = args.storageDir
            }

            if (args.storageFile.substr(-1, 1) !== '/') {
                args.storageFile += '/'
            }

            // For #load
            args.storageFile += NodejsStore.#storageFilename
        }

        super(args, debug)

        // For #persist. Can't have this before the super call. Therefore all that dancing.
        this.#storageFile = args.storageFile
    }

    /**
     * @param {Object.<string, any>} args
     * @return {undefined|StoreData}
     */
    load(args) {
        if (NodejsStore.#isReadable(args.storageFile)) {
            return JSON.parse(fs.readFileSync(args.storageFile, 'utf8'))
        }

        return undefined
    }

    /**
     * @param {boolean} async
     * @param {function(?Error=)} done
     */
    persist(async, done) {
        const data = JSON.stringify(this)

        if (async) {
            fs.writeFile(this.#storageFile, data, 'utf8', () => { done() })
        }
        else {
            fs.writeFileSync(this.#storageFile, data, 'utf8')
            done()
        }
    }

    /**
     * @param {string} data
     * @param {string} server
     * @param {number} timeout
     * @param {function(?Error=)} done
     */
    send(data, server, timeout, done) {
        const req = http.request(server, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Content-Length': Buffer.byteLength(data)
            },
            timeout: timeout * 1000
        }, (res) => {
            let body = ''

            res.on('data', (chunk) => {
                body += chunk.toString()
            })

            res.on('end', () => {
                if (res.statusCode !== 200 && res.statusCode !== 204) {
                    return done(new Error(`HTTP Error ${res.statusCode}: ${body}`))
                }

                done()
            })
        })

        req.on('error', (error) => {
            done(error)
        })

        req.write(data)
        req.end()
    }

    /**
     * @param {string} path
     * @return {boolean}
     */
    static #isReadable(path) {
        try {
            fs.accessSync(path, fs.constants.F_OK)

            return true
        }
        catch (err) {
            return false
        }
    }
}

module.exports = NodejsStore
