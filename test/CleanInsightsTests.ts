import {
    BrowserStore,
    Campaign,
    CleanInsights,
    Configuration, ConfigurationData,
    Consent, ConsentRequestUi,
    Consents, ConsentState,
    Event,
    EventAggregationRule,
    Feature,
    Insights,
    Store,
    StoreData,
    Visit
} from '../lib'
import {beforeEach, describe, it} from 'mocha'
import {expect} from 'chai'

import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'

dayjs.extend(utc)


const confJson = {
    "server": "http://localhost:8080/ci/cleaninsights.php",
    "siteId": 1,
    "timeout": 1,
    "maxRetryDelay": 1,
    "maxAgeOfOldData": 1,
    "persistEveryNTimes": 1,
    "serverSideAnonymousUsage": false,
    "debug": true,
    "campaigns": {
        "test": {
            "start": "2021-01-01T00:00:00-00:00",
            "end": "2099-12-31T23:59:59-00:00",
            "aggregationPeriodLength": 1,
            "numberOfPeriods": 90,
            "onlyRecordOnce": false,
            "eventAggregationRule": "avg",
            "strengthenAnonymity": false
        }
    }
}

const conf = new Configuration('http://localhost:8080/ci/cleaninsights.php',
    1,
    {
        "test": new Campaign(
            dayjs('2021-01-01T00:00:00-00:00'),
            dayjs('2099-12-31T23:59:59-00:00'),
            1,
            90,
            false,
            EventAggregationRule.Avg,
            false)
    },
    1,
    1,
    1,
    1,
    false,
    true)


let ci: CleanInsights

beforeEach(() => {
    let store = getStoredStore()
    store.consents = new Consents()
    store.visits = []
    store.events = []

    store.persist(false, () => {})

    ci = new CleanInsights(confJson)
})


describe('CleanInsights', () => {

    it('reads the configuration correctly', () => {
        expect(ci.conf).to.deep.equal(conf)
    })

    it('denies consent correctly', () => {
        expect(ci.stateOfFeature(Feature.lang)).to.equal(ConsentState.unknown)
        expect(ci.stateOfFeature(Feature.ua)).to.equal(ConsentState.unknown)
        expect(ci.stateOfCampaign("test")).to.equal(ConsentState.unknown)

        ci.denyFeature(Feature.lang)
        ci.denyFeature(Feature.ua)
        ci.denyCampaign("test")

        expect(ci.featureConsents.length).to.equal(2)
        expect(ci.campaignConsents.length).to.equal(1)

        let consent: Consent|null = ci.getFeatureConsentByIndex(0)
        expect(consent).to.not.equal(null)
        expect(consent?.granted).to.be.false
        expect(ci.stateOfFeature(Feature.lang)).to.equal(ConsentState.denied)

        consent = ci.getFeatureConsentByIndex(1)
        expect(consent).to.not.equal(null)
        expect(consent?.granted).to.be.false
        expect(ci.stateOfFeature(Feature.ua)).to.equal(ConsentState.denied)

        consent = ci.getFeatureConsentByIndex(2)
        expect(consent).to.equal(null)

        consent = ci.getCampaignConsentByIndex(0)
        expect(consent).to.not.equal(null)
        expect(consent?.granted).to.be.false

        consent = ci.getCampaignConsentByIndex(1)
        expect(consent).to.equal(null)

        expect(ci.isCampaignCurrentlyGranted('test')).to.be.false
        expect(ci.stateOfCampaign("test")).to.equal(ConsentState.denied)
    })

    it('grants consent correctly', () => {
        expect(ci.stateOfFeature(Feature.lang)).to.equal(ConsentState.unknown)
        expect(ci.stateOfFeature(Feature.ua)).to.equal(ConsentState.unknown)
        expect(ci.stateOfCampaign("test")).to.equal(ConsentState.unknown)

        ci.grantFeature(Feature.lang)
        ci.grantFeature(Feature.ua)
        ci.grantCampaign('test')

        expect(ci.featureConsents.length).to.equal(2)
        expect(ci.campaignConsents.length).to.equal(1)

        let consent: Consent|null = ci.getFeatureConsentByIndex(0)
        expect(consent).to.not.equal(null)
        expect(consent?.granted).to.be.true
        expect(ci.stateOfFeature(Feature.lang)).to.equal(ConsentState.granted)

        consent = ci.getFeatureConsentByIndex(1)
        expect(consent).to.not.equal(null)
        expect(consent?.granted).to.be.true
        expect(ci.stateOfFeature(Feature.ua)).to.equal(ConsentState.granted)

        consent = ci.getFeatureConsentByIndex(2)
        expect(consent).to.equal(null)


        let midnightToday = dayjs().startOf('day')

        consent = ci.getCampaignConsentByIndex(0)
        expect(consent).to.not.equal(null)
        expect(consent?.start.isAfter(midnightToday)).to.be.true
        expect(consent?.granted).to.be.true

        consent = ci.getCampaignConsentByIndex(1)
        expect(consent).to.equal(null)

        expect(ci.isCampaignCurrentlyGranted('test')).to.be.true
        expect(ci.stateOfCampaign("test")).to.equal(ConsentState.granted)
    })

    it('grants consent correctly with strengthened anonymity', () => {
        expect(ci.stateOfFeature(Feature.lang)).to.equal(ConsentState.unknown)
        expect(ci.stateOfFeature(Feature.ua)).to.equal(ConsentState.unknown)
        expect(ci.stateOfCampaign("test")).to.equal(ConsentState.unknown)

        ci = getStrengthenedCi()

        ci.grantFeature(Feature.lang)
        ci.grantFeature(Feature.ua)
        ci.grantCampaign('test')

        expect(ci.featureConsents.length).to.equal(2)
        expect(ci.campaignConsents.length).to.equal(1)

        let consent: Consent|null = ci.getFeatureConsentByIndex(0)
        expect(consent).to.not.equal(null)
        expect(consent?.granted).to.be.true
        expect(ci.stateOfFeature(Feature.lang)).to.equal(ConsentState.granted)

        consent = ci.getFeatureConsentByIndex(1)
        expect(consent).to.not.equal(null)
        expect(consent?.granted).to.be.true
        expect(ci.stateOfFeature(Feature.ua)).to.equal(ConsentState.granted)

        consent = ci.getFeatureConsentByIndex(2)
        expect(consent).to.equal(null)


        let midnightTomorrow = dayjs().startOf('day').add(1, 'day')

        consent = ci.getCampaignConsentByIndex(0)
        expect(consent).to.not.equal(null)
        expect(consent?.start.isAfter(midnightTomorrow)).to.be.true
        expect(consent?.granted).to.be.true

        consent = ci.getCampaignConsentByIndex(1)
        expect(consent).to.equal(null)

        expect(ci.isCampaignCurrentlyGranted('test')).to.be.false
        expect(ci.stateOfCampaign("test")).to.equal(ConsentState.notStarted)
    })

    it('handles invalid campaign correctly', () => {
        expect(ci.consentForCampaign("foobar")).to.be.null

        expect(ci.stateOfCampaign("foobar")).to.equal(ConsentState.unconfigured)

        ci.requestConsentForCampaign("foobar", new ConsentRequestUi(), (granted) => {
            expect(granted).to.be.false
        })

        expect(ci.grantCampaign("foobar")).to.be.null

        expect(ci.denyCampaign("foobar")).to.be.null

        expect(ci.campaignConsents.length).to.equal(0)

        expect(ci.isCampaignCurrentlyGranted("foobar")).to.be.false
    })

    it('persists correctly', () => {
        ci.grantFeature(Feature.lang)
        ci.grantFeature(Feature.ua)
        ci.grantCampaign('test')

        expect(ci.isCampaignCurrentlyGranted('test')).to.be.true

        ci.measureVisit(['foo'], 'test')
        ci.measureEvent('foo', 'bar', 'test', 'baz', 4567)

        ci.persist()

        let store = getStoredStore()

        let midnight = dayjs().utc().startOf('day').local()

        let midnightTomorrow = dayjs().utc().startOf('day').local().add(1, "day")

        expect(store.visits).to.deep.equal([new Visit(['foo'], 'test', 1, midnight, midnightTomorrow)])
        expect(store.events).to.deep.equal([new Event('foo', 'bar', 'baz', 4567,
            'test', 1, midnight, midnightTomorrow)])
    })

    it('persists correctly with strengthened anonymity', () => {
        ci = getStrengthenedCi()

        ci.grantFeature(Feature.lang)
        ci.grantFeature(Feature.ua)
        ci.grantCampaign('test')

        ci.measureVisit(['foo'], 'test')
        ci.measureEvent('foo', 'bar', 'test', 'baz', 4567)

        ci.persist()

        let store = getStoredStore()

        expect(store.consents).to.deep.equal(ci['store'].consents)
        expect(store.visits).to.deep.equal([]) // Consent will only start tomorrow!
        expect(store.events).to.deep.equal([]) // Consent will only start tomorrow!

        // Re-init with faked store.
        ci = new CleanInsights(confJson, fakeYesterdayConsent())

        expect(ci.isCampaignCurrentlyGranted('test')).to.be.true

        ci.measureVisit(['foo'], 'test')
        ci.measureEvent('foo', 'bar', 'test', 'baz', 4567)

        ci.persist()

        store = getStoredStore()

        let midnight = dayjs().utc().startOf('day').local()

        let midnightTomorrow = dayjs().utc().startOf('day').local().add(1, "day")

        expect(store.visits).to.deep.equal([new Visit(['foo'], 'test', 1, midnight, midnightTomorrow)])
        expect(store.events).to.deep.equal([new Event('foo', 'bar', 'baz', 4567,
            'test', 1, midnight, midnightTomorrow)])
    })

    it('purges correctly', () => {
        const store = new TestStore()

        const dayBeforeYesterday = dayjs().subtract(dayjs.duration(2, 'days'))

        store.visits.push(new Visit(['foo'], "x", 1, dayBeforeYesterday, dayBeforeYesterday))
        store.events.push(new Event('foo', 'bar', undefined, undefined, 'x',
            1, dayBeforeYesterday, dayBeforeYesterday))

        TestInsights.purge(conf, store)

        expect(store.visits.length).to.equal(0, 'Should have been purged.')
        expect(store.events.length).to.equal(0, 'Should have been purged.')

        let now = dayjs()

        store.visits.push(new Visit(['foo'], 'x', 1, now, now))
        store.events.push(new Event('foo', 'bar', undefined, undefined, 'x',
            1, now, now))

        TestInsights.purge(conf, store)

        expect(store.visits.length).to.equal(1, 'Should not have been purged.')
        expect(store.events.length).to.equal(1, 'Should not have been purged.')
    })

    it('serializes insights correctly', () => {
        let insights = new Insights(conf, new TestStore());

        expect(JSON.stringify(insights)).to.equal("{\"idsite\":1,\"visits\":[],\"events\":[]}")

        const store = new TestStore()

        store.visits.push(new Visit(["foo", "bar"], "test", 1,
            dayjs("2022-01-01T00:00:00Z"), dayjs("2022-01-02T00:00:00Z")))

        store.events.push(new Event("foo", "bar", "baz", 6.66, "test", 1,
            dayjs("2022-01-01T00:00:00Z"), dayjs("2022-01-02T00:00:00Z")))

        const c = new Configuration(conf.server, conf.siteId, conf.campaigns, conf.timeout, conf.maxRetryDelay,
            30000, conf.persistEveryNTimes, conf.serverSideAnonymousUsage, conf.debug)

        insights = new Insights(c, store)

        expect(JSON.stringify(insights)).to.equal("{\"idsite\":1,\"visits\":[{\"action_name\":\"foo/bar\",\"period_start\":1640995200,\"period_end\":1641081600,\"times\":1}],\"events\":[{\"category\":\"foo\",\"action\":\"bar\",\"name\":\"baz\",\"value\":6.66,\"period_start\":1640995200,\"period_end\":1641081600,\"times\":1}]}")
    })

    it('can test server', (done) => {
        ci = new CleanInsights(confJson, new TestStore())

        ci.testServer((error?: Error) => {
            expect(error).to.be.undefined
            done()
        })
    })

    it('can tell invalid configs', () => {
        testConfigCheck({server: "", siteId: -1, campaigns: {}}, false)

        testConfigCheck({server: "", siteId: -1, campaigns: {"foobar": {start: "", end: "", aggregationPeriodLength: 0}}}, false)

        testConfigCheck({server: "", siteId: 1, campaigns: {"foobar": {start: "", end: "", aggregationPeriodLength: 0}}}, false)

        testConfigCheck({server: "https://example.org/", siteId: 1, campaigns: {"foobar": {start: "", end: "", aggregationPeriodLength: 0}}}, true)
    })
})

function getStoredStore(): BrowserStore {
    return new BrowserStore()
}

function getStrengthenedCi(): CleanInsights {
    let co = conf
    let ca = conf.campaigns["test"]

    return new CleanInsights(new Configuration(
        co.server,
        co.siteId,
        {
            "test": new Campaign(
                ca.start,
                ca.end,
                ca.aggregationPeriodLength,
                ca.numberOfPeriods,
                ca.onlyRecordOnce,
                ca.eventAggregationRule,
                true)
        },
        co.timeout,
        co.maxRetryDelay,
        co.maxAgeOfOldData,
        co.persistEveryNTimes,
        co.serverSideAnonymousUsage,
        co.debug))
}

function fakeYesterdayConsent(): BrowserStore {
    let store = getStoredStore()

    let yesterday = dayjs().subtract(1, 'day')

    let end = dayjs().add(3, 'day')

    store.consents.campaigns['test'] = new Consent(true, yesterday, end)

    return store
}

function testConfigCheck(data: ConfigurationData, isGood: boolean) {
    let conf = new Configuration(data)

    let count = 0

    let result = conf.check(() => {
        count++
    })

    if (isGood) {
        expect(count).to.equal(0)
        expect(result).to.be.true
    }
    else {
        expect(count).to.equal(1)
        expect(result).to.be.false
    }
}

class TestStore extends Store {

    load(args: { [p: string]: any }): StoreData | undefined {
        return undefined
    }

    persist(async: boolean, done: (error?: Error) => void): void {
        done(new Error("foobar!"))
    }

    send(data: string, server: string, timeout: number, done: (error?: Error) => void): void {
        setTimeout(() => {
            done(new Error(`HTTP Error 400: Bad Request`))
        }, 50)
    }
}

class TestInsights extends Insights {

    public static purge(conf: Configuration, store: Store) {
        super.purge(conf, store)
    }
}
