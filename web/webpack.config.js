const path = require('path')

const {NODE_ENV = 'production'} = process.env

module.exports = {
    entry: './web/clean-insights-auto-tracker.ts',
    mode: NODE_ENV,
    output: {
        filename: 'clean-insights-auto-tracker.js',
        path: path.resolve(__dirname, '..')
    },
    optimization: {
        minimize: true
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            loader: 'ts-loader',
        }]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    }
}