import {CampaignData, CleanInsights, Feature} from '../lib'

declare global {
    // noinspection JSUnusedGlobalSymbols
    interface Window {
        CleanInsightsAutoTracker: (server: string, siteId: number) => CleanInsights,
        cleanInsights: CleanInsights|undefined
        CleanInsightsFeature: {[p: string]: string}
    }
}

/**
 * Instantiates a `CleanInsights` object with a default configuration and a default campaign
 * named "visits" which never expires.
 *
 * The "visits" campaign will have an `aggregationPeriodLength` of 1 day. That means, visits
 * will be collected during 24 hours and then sent to the server, evenly distributed over
 * that day, in order to keep user privacy.
 *
 * After instantiation, it will automatically track a visit to the current page, using the
 * URL path as its identifier, but ONLY, if consent is given.
 *
 * You will need to show some consent form to the user (i.e. a "cookie banner"), so the user
 * is able to agree to this tracking before setting consent.
 *
 * Refer to the website example code to see how consent is handled, or the frontend example
 * code for a more advanced usage.
 *
 * The instantiated object is returned and also attached to `window.cleanInsights` for
 * advanced use.
 *
 * @param {string} server
 *      The server URL should look like `https://myhost.example.com/ci/cleaninsights.php`.
 *
 * @param {number} siteId
 *      The Matomo site ID to record page visits for.
 *
 * @param {Object.<string, Campaign>=} campaigns
 *      (Additional) campaign configurations. OPTIONAL
 *
 * @constructor
 */
window.CleanInsightsAutoTracker = (server: string, siteId: number, campaigns?: { [p: string]: CampaignData }): CleanInsights => {

    const conf = {
        "server": server,
        "siteId": siteId,
        "persistEveryNTimes": 1, // Always persist. Local storage should be fast enough to do that.
        "campaigns": campaigns || {}
    };

    if (!conf.campaigns.hasOwnProperty('visits')) {
        conf.campaigns.visits = {
            "start": "1970-01-01T00:00:00-00:00",
            "end": "2099-12-31T23:59:59-00:00",
            "aggregationPeriodLength": 1,
            "numberOfPeriods": 50000
        }
    }

    const ci = new CleanInsights(conf)

    // Measure page visits by default.
    ci.measureVisit(document.location.pathname.split('/'), 'visits')

    window.cleanInsights = ci

    return ci
}

window.CleanInsightsFeature = Feature