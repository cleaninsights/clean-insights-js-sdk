# Clean Insights SDK

## 2.6.2
- Updated dependencies.
- Moved from the `store-js` dependency, which was a discontinued fork
  of `Store.js` to the original, updated fork of `Store.js` (just named 'store').

## 2.6.1
- Added more debug logging when sending data to help understand why send fails.
- Added missing error handling on persist error.

## 2.6.0  

- Added `consentForFeature`, `consentForCampaign` and `stateOfFeature`, `stateOfCampaign`
  methods to get detailed consent information.

## 2.5.1

- Fixed wrong JSON serialization of data sent to server.

## 2.5.0

- Relaxed anonymity guarantees somewhat by allowing to start measuring right away by default,
  not just at the next period. Added "strengthenAnonymity" toggle to switch back to old behaviour.
- Updated dependencies.

## 2.4.4

- Added `Configuration#check` to check for most common configuration mistakes.

## 2.4.3

- Added `testServer` helper method to `CleanInsights` object, to test for server issues without needing to go through 
  a full measurement cycle.

## 2.4.2

- Reversed logic in #measureVisit and #measureEvent methods, to make absolutely sure, that #persistAndSend gets called.
- Updated dependencies.

## 2.4.1

- Export `CampaignData` object.
- Added `AutoTracker` prepackaged script. (Not contained in npm distro, just in the repository.)

## 2.4.0

- Added truncated exponential backoff retries on server failures.
- Added automatic purge of too old unsent data.

## 2.3.1

- Fixed build.

## 2.3.0

- Added some unit tests.

## 2.2.1

- Initial working release. 
